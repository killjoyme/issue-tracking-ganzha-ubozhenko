#ifndef MODULE_H
#define MODULE_H
#include <iostream>
#include <ctime>
using namespace std;

struct products {
	string name;
	int id;
	int price;
	int quantity;
};

class Files {
	friend class Furniture;
private:
	fstream file;
public:
	void fileRecording(Furniture& ware);
	void listLoading(Furniture& ware);
};

void Files::fileRecording(Furniture& ware) {
	file.open("data.xls", ios::app);
	for (int i = 0; i < ware.size; i++) {
		file << ware.goodsList[i].name << "\t" << ware.goodsList[i].id << "\t" << ware.goodsList[i].price << "\t" << ware.goodsList[i].quantity << "\n";
	}
	file.close();
}

void Files::listLoading(Furniture& ware) {
	file.open("data.xls", ios::in);
	for (int i = 0; i < ware.size; i++) {
		file >> ware.goodsList[i].name >> ware.goodsList[i].id >> ware.goodsList[i].price >> ware.goodsList[i].quantity;
		cout << ware.goodsList[i].name << "\t" << ware.goodsList[i].id << "\t" << ware.goodsList[i].price << "\t" << ware.goodsList[i].quantity << endl;
	}
	file.close();
}

class Person {
	friend class Furniture;
private:
	int amountOfNumbers;
	int* numbersOfGoods;
	products* orders;
	string fName, lName;
public:
	void logining();
	void viewOrders();
	void freeUpMemory();
	void makeOrder(Furniture& ware);
	void deleteOrder(Furniture& ware);
};

void Person::logining() {
	cout << "���� ��� - ";
	cin >> fName;
	cout << "���� ������� - ";
	cin >> lName;
	cout << "�������!\n";
	system("pause");
	system("cls");
}

void Person::viewOrders() {
	cout << fName << ", ��� ������ �������: " << endl;
	for (int i = 0; i < amountOfNumbers; i++) {
		cout << i + 1 << ". ����� - " << orders[i].name << "\t���� - " << orders[i].price << "\t���������� - " << orders[i].quantity << endl;
	}
	int total = 0;
	for (int i = 0; i < amountOfNumbers; i++) {
		total += (orders[i].quantity * orders[i].price);
	}
	cout << "����� - " << total << " Rub." << endl;
}

void Person::makeOrder(Furniture& ware) {
	ware.listOutput();
	cout << "������� ������� �� ������ ��������? >>> ";
	cin >> amountOfNumbers;
	numbersOfGoods = new int[amountOfNumbers];
	orders = new products[amountOfNumbers];
	for (int i = 0; i < amountOfNumbers; i++) {
		cout << "������� ����� ������ " << i + 1 << " >>> ";
		cin >> numbersOfGoods[i];
		cout << "������� ���������� ������ " << i + 1 << " >>> ";
		cin >> orders[i].quantity;
	}

	for (int i = 0; i < amountOfNumbers; i++) {
		orders[i].name = ware.goodsList[numbersOfGoods[i] - 1].name;
		orders[i].id = ware.goodsList[numbersOfGoods[i] - 1].id;
		orders[i].price = ware.goodsList[numbersOfGoods[i] - 1].price;
		ware.goodsList[numbersOfGoods[i] - 1].quantity -= orders[i].quantity;
	}

}

void Person::deleteOrder(Furniture& ware) {
	viewOrders();
	int orderToDelete;
	cout << "������� ����� ���������� ������ >>> ";
	cin >> orderToDelete;
	products* temp = new products[amountOfNumbers - 1];
	for (int i = 0; i < orderToDelete - 1; i++) {
		temp[i].name = orders[i].name;
		temp[i].id = orders[i].id;
		temp[i].price = orders[i].id;
	}
	for (int i = orderToDelete - 1; i < amountOfNumbers; i++) {
		temp[i].name = orders[i + 1].name;
		temp[i].id = orders[i + 1].id;
		temp[i].price = orders[i + 1].id;
	}
	orders = new products[amountOfNumbers - 1];
	for (int i = 0; i < amountOfNumbers - 1; i++) {
		orders[i].name = temp[i].name;
		orders[i].id = temp[i].id;
		orders[i].price = temp[i].price;
	}
	delete[] temp;
	viewOrders();
}

class Furniture {
private:
	int size;

public:
	products* goodsList;
	void listCreating();
	void listOutput();
	void memoryClearing();
	void addGoods();
};
void Furniture::listCreating() {
	cout << "������� ���������� ����������� ������� >>> ";
	cin >> size;
	goodsList = new products[size];
	srand(time(0));

	for (int i = 0; i < size; i++) {
		cout << "������� ��� ������ " << i + 1 << " >>> ";
		cin >> goodsList[i].name;
		goodsList[i].id = (rand()) / 100;
		cout << "������� ��������� ������ " << i + 1 << " >>> ";
		cin >> goodsList[i].price;
		cout << "������� ���������� ������ " << i + 1 << " >>> ";
		cin >> goodsList[i].quantity;
	}
	cout << "������ ������!" << endl;
}

void Furniture::listOutput() {
	for (int i = 0; i < size; i++) {
		cout << i + 1 << ". ��� - " << goodsList[i].name << "\tID - " << goodsList[i].id << "\t���� - " << goodsList[i].price << "\t���-�� - " << goodsList[i].quantity << "" << endl;
	}
}

void Furniture::memoryClearing() {
	delete[] goodsList;
}

void Furniture::addGoods() {
	products* arr1;
	int sizeNew;
	cout << "������� ������� �� ������ ��������? >>> ";
	cin >> sizeNew;
	arr1 = new products[size + sizeNew];
	for (int i = 0; i < size; i++) {
		arr1[i].name = goodsList[i].name;
		arr1[i].id = goodsList[i].id;
		arr1[i].price = goodsList[i].price;
	}

	for (int i = size; i < size + sizeNew; i++) {
		cout << "������� ��� ������ " << i + 1 << " >>>\n";
		cin >> arr1[i].name;
		arr1[i].id = (rand()) / 100;
		cout << "������� ��������� ������ " << i + 1 << " >>> ";
		cin >> arr1[i].price;
		cout << "������� ���������� ������ " << i + 1 << " >>> ";
		cin >> arr1[i].quantity;
	}
	size += sizeNew;
	goodsList = new products[size];
	for (int i = 0; i < size; i++) {
		goodsList[i].name = arr1[i].name;
		goodsList[i].id = arr1[i].id;
		goodsList[i].price = arr1[i].price;
		goodsList[i].quantity = arr1[i].quantity;
	}
	cout << "�����(�) ��������(�)!\n";
	delete[] arr1;
}

#endif 