﻿#include <iostream>
#include <cstdlib>
#include <string>
#include <time.h>
#include "module.h"
void mainMenu() {
	cout << "1. Товары.\n"
		<< "2. Мои заказы.\n"
		<< "3. Сделать заказ.\n"
		<< "4. Отменить заказ.\n"
		<< "5. Загрузить список товаров.\n"
		<< "6. Выйти.\n"
		<< ">>> ";
}

void menuOutput() {
	cout << "1. Создать список товаров.\n"
		<< "2. Просмотреть список товаров.\n"
		<< "3. Добавление товаров.\n"
		<< "4. Выйти.\n"
		<< ">>> ";

}

int main() {
	setlocale(0, "rus");
	Furniture ware;
	Person client;
	Files file;
	client.logining();
	int key;
	int key1;
menu:
	mainMenu();
	cin >> key;
	for (int i = 1; ; i++) {
		switch (key) {
		case 1:
			system("cls");
			menuOutput();
			cin >> key1;
			switch (key1) {
			case 1:
				system("cls");
				ware.listCreating();
				file.fileRecording(ware);
				system("pause");
				system("cls");
				break;
			case 2:
				system("cls");
				ware.listOutput();
				system("pause");
				system("cls");
				break;
			case 3:
				system("cls");
				ware.addGoods();
				system("pause");
				system("cls");
				break;
			case 4:
				break;
			default:
				cerr << "Вы выбрали неверный вариант :(\n";
				system("pause");
				system("cls");
			}
			system("cls");
			break;
		case 2:
			system("cls");
			client.viewOrders();
			system("pause");
			system("cls");
			break;
		case 3:
			system("cls");
			client.makeOrder(ware);
			system("pause");
			system("cls");
			break;
		case 4:
			system("cls");
			client.deleteOrder(ware);
			system("pause");
			system("cls");
			break;
		case 5:
			system("cls");
			file.listLoading(ware);
			ware.listOutput();
			system("pause");
			system("cls");
			break;
		case 6:
			cout << "Выход из программы...";
			ware.memoryClearing();
			client.freeUpMemory();
			exit(EXIT_SUCCESS);
		default:
			cerr << "Вы выбрали неверный вариант :(\n";
			system("pause");
			system("cls");
		}
		goto menu;
	}
	system("pause");
	return 0;
}